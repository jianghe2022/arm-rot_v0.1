#include "stm32f10x.h"                  // Device header
#include "Delay.h"

void LED_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	
	GPIO_InitTypeDef led_gpio;
	led_gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	led_gpio.GPIO_Pin = GPIO_Pin_13;
	led_gpio.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(GPIOC, &led_gpio);
	GPIO_SetBits(GPIOC,GPIO_Pin_13);
}

void LED_Blink(void)
{
	GPIO_SetBits(GPIOC,GPIO_Pin_13);
	Delay_ms(500);
	GPIO_ResetBits(GPIOC,GPIO_Pin_13);
	Delay_ms(500);
}

void LED_ON(void)
{
	GPIO_ResetBits(GPIOC,GPIO_Pin_13);
}

void LED_OFF(void)
{
	GPIO_SetBits(GPIOC,GPIO_Pin_13);
}
