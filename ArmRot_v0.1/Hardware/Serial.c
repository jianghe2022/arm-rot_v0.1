#include "stm32f10x.h"                  // Device header
#include <stdio.h>
#include <stdarg.h>
#include "Serial.h"

uint8_t Serial_TxPacket[4];	//HEX
//uint8_t Serial_RxPacket[4];	//HEX
char Serial_RxPacket[100];		//字符串
uint8_t Serial_RxFlag;

void Serial_Init(void)
{
	//开启时钟：USART2和GPIOA
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	//参数配置：GPIO
	GPIO_InitTypeDef uart_gpio;
	
	uart_gpio.GPIO_Mode = GPIO_Mode_AF_PP;	//推挽输出 Txd
	uart_gpio.GPIO_Pin = GPIO_Pin_2;
	uart_gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &uart_gpio);
	
	uart_gpio.GPIO_Mode = GPIO_Mode_IPU;	//上拉输入 Rxd
	uart_gpio.GPIO_Pin = GPIO_Pin_3;
	uart_gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &uart_gpio);
	
	//参数配置：USART
	USART_InitTypeDef usart_initstruture;
	usart_initstruture.USART_BaudRate = 9600;
	usart_initstruture.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	usart_initstruture.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;
	usart_initstruture.USART_Parity = USART_Parity_No; 
	usart_initstruture.USART_StopBits = USART_StopBits_1; 
	usart_initstruture.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART2,&usart_initstruture);
	
	//开启串口中断-当需要接收外来数据时用到
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	
	//参数配置：NVIC
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);		//分组
	
	NVIC_InitTypeDef nvic_initstructure;				//参数配置
	nvic_initstructure.NVIC_IRQChannel = USART2_IRQn;
	nvic_initstructure.NVIC_IRQChannelCmd = ENABLE;
	nvic_initstructure.NVIC_IRQChannelPreemptionPriority = 1;
	nvic_initstructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_Init(&nvic_initstructure);
	
	//开启：USART
	USART_Cmd(USART2, ENABLE);
}

/**
  * @brief 发送一个字节
  * @param byte 0-255
  * @retva NO
  */
void Serial_SendByte(uint8_t byte)
{
	USART_SendData(USART2, byte);
	while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
}

/**
  * @brief 发送字符串
  * @param *string 0-255
  * @retva NO
  */
void Serial_SendString(char *string)
{
	uint8_t i;
	for(i=0;string[i]!='\0';i++)
	{
		Serial_SendByte(string[i]);
	}
}

/**
  * @brief 发送数组
  * @param *array,length 最长是0-65535
  * @retva NO
  */
void Serial_SendArrary(uint8_t *array, uint16_t length)
{
	uint16_t i;
	for(i=0;i<length;i++)
	{
		Serial_SendByte(array[i]);
	}
}

/**
  * @brief 幂函数pow
  * @param x,y 2的32次方
  * @retva NO
  */
uint32_t Serial_Pow(uint32_t x, uint32_t y)
{
	uint32_t result = 1;
	while(y--)
	{
		result *= x;
	}
	return result;
}
/**
  * @brief 发送数字
  * @param number,length 发送数字及其长度
  * @retva NO
  */
void Serial_SendNumber(uint32_t number, uint8_t length)
{
	uint8_t i;
	for(i=0; i<length; i++)
	{
		Serial_SendByte(number / Serial_Pow(10,length-i-1) % 10 + '0');
	}
}

/**
  * @brief 重定向fputc,使得print打印输出到串口
  * @param ch,*f
  * @retva ch
  */
int fputc(int ch, FILE *f)
{
	Serial_SendByte(ch);
	return ch;
}

void Serial_Printf(char *format, ...)
{
	char string[100];
	va_list arg;
	va_start(arg, format);
	vsprintf(string, format, arg);
	va_end(arg);
	Serial_SendString(string);
}

//HEX数据包
void Serial_SendPacket(void)
{
	Serial_SendByte(0xFF);
	Serial_SendArrary(Serial_TxPacket, 4);
	Serial_SendByte(0xFE);
}

uint8_t Serial_GetRxFlag(void)
{
	if(Serial_RxFlag == 1)
	{
		Serial_RxFlag = 0;
		return 1;
	}
	return 0;
}

void USART2_IRQHandler(void)
{
	static uint8_t RxState = 0;
	static uint8_t pRxPacket = 0;
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		uint8_t RxData = USART_ReceiveData(USART2);
		if(RxState == 0)
		{
			if(RxData == '@')
			{
				RxState = 1;
				pRxPacket = 0;
			}
		}
		else if(RxState == 1)
		{
			if(RxData == '\r')
			{
				RxState = 2;
			}
			else
			{
				Serial_RxPacket[pRxPacket] = RxData;
				pRxPacket ++;
			}
		}
		else if(RxState == 2)
		{
			if(RxData == '\n')
			{
				RxState = 0;
				Serial_RxPacket[pRxPacket] = '\0';
				Serial_RxFlag = 1;
			}
		}
		USART_ClearITPendingBit(USART2, USART_IT_RXNE);
		
		
	}
}

