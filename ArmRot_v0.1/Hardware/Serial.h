#ifndef __SERIAL_H
#define __SERIAL_H

#include <stdio.h>

extern char Serial_RxPacket[];

void Serial_Init(void);
void Serial_SendByte(uint8_t byte);
void Serial_SendString(char *string);
void Serial_SendArrary(uint8_t *array, uint16_t length);
void Serial_SendNumber(uint32_t number, uint8_t length);
void Serial_Printf(char *format, ...);

void Serial_SendPacket(void);

uint8_t Serial_GetRxFlag(void);

#endif
