#ifndef __SERVO_H
#define __SERVO_H

void Servo_Init(void);
void Servo0_SetAngle(float Angle);
void Servo1_SetAngle(float Angle);
void Servo2_SetAngle(float Angle);
void Servo3_SetAngle(float Angle);
void Servo_SetAngle(float A1,float A2,float A3,float A4);

#endif
