#ifndef __INVERSEKINEMATIC_H
#define __INVERSEKINEMATIC_H
//逆运动学解-笛卡尔坐标系

#include <stdio.h>
#include <math.h>
//inversekinematic

//测量出3自由度机械臂各个关节长度
/* 底座高度
 * 前臂长度
 * 后臂长度
 * 抓手长度
 */
#define Length0_Base 50
#define Length1_ForeArm 110
#define Length2_RearArm 120
#define Length3_Gripper 50

#define PI 3.14159265

int inverseKinematics(void);


#endif
