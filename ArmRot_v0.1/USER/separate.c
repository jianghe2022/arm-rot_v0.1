#include "stm32f10x.h"                  // Device header
#include "separate.h"
#include "Serial.h"

//char str[100] = "";  //单片机从上位机接收到的字符串数据

char *strX, *strY, *strZ;   //定义字符串：X Y Z
char *receive[3] = {"0","0","0"};

float floatX, floatY, floatZ;        //定义浮点型坐标x y z

//字符串的分离：从'( ,' - ', ,' - ', )'中只提取数字、负号、小数点三种字符，其他字符当作分割符
//遍历字符串、分隔符采用字符匹配、各自放入strX Y Z中
int separate_X_Y_Z(void)
{
	
    char buf[STR_LEN+1] = "0";
    //第0位不要，最后面的‘)’不要
    
    for(int i=1;Serial_RxPacket[i] != '\0';i++)
    {
        if(Serial_RxPacket[i]!=')')
        {
            buf[i-1] = Serial_RxPacket[i];
        }
    }

    
    // char buf[]="120.11,-100.44,200.0978";
    //以逗号为分隔符分开各个数据
    char *temp = strtok(buf,",");
    receive[0]=temp;
    if(temp != NULL)
    {
        for(int i=1;i<3; i++)
        {
            temp = strtok(NULL,",");
            receive[i]=temp;
        }
    }


    strX = receive[0];
    strY = receive[1];
    strZ = receive[2];


    floatX = atof(strX);
    floatY = atof(strY);
    floatZ = atof(strZ);


    Serial_Printf( "x=%.4lf\r\n",  floatX);
    Serial_Printf( "y=%.4lf\r\n",  floatY);
    Serial_Printf( "z=%.4lf\r\n",  floatZ);
	
	return 0; 
}

