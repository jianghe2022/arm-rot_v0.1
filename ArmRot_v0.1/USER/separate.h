#ifndef __SEPARATE_H
#define __SEPARATE_H

#include <stdio.h>
#include <stdlib.h>
// #include <ctype.h>
#include <string.h>

#define STR_LEN 100

extern float floatX, floatY, floatZ;        //定义浮点型坐标x y z

int separate_X_Y_Z(void);

#endif
