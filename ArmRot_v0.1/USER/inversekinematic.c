#include "stm32f10x.h"                  // Device header
#include "inversekinematic.h"
#include "Serial.h"
#include "separate.h"
#include "servo.h"
#include "Delay.h"

float Length0 = Length0_Base;
float Length1 = Length1_ForeArm;
float Length2 = Length2_RearArm;
float Length3 = Length3_Gripper;

float Aux;

/***********************************/
float Angle0, Angle1, Angle2;
float Angle3=90;
float Angle11, Angle12;
float val = 180.0 / PI;     //弧度值转角度值
/***********************************/

float Projection = 0;

//float floatX = -50;
//float floatY = 70;
//float floatZ = 2;

/*
 *  已知（x, y, z）= (-60, 80, 50)
 *  已知 Length0, Length1, Length2, Length3
 *  求 Angle0, Angle1, Angle2   弧度值
**/
//笛卡尔坐标系求逆运动学解
/***********************************/
int inverseKinematics(void)
{
    //计算投影长度
    Projection = sqrtf((floatX)*(floatX)+(floatY)*(floatY));

    Angle0 = atan(floatY/floatX);
    Angle0 = Angle0*val;    //弧度值转角度值
    
    if(floatZ == Length0)
    {        
        // printf("=");
        Angle1 = acos( (Length1*Length1 + Projection*Projection - 2*Length1*Projection) / ((Length2+Length3)*(Length2+Length3)) );
        Angle1 = Angle1*val;    //弧度值转角度值

        Angle2 = acos( (Length1*Length1 + ((Length2+Length3)*(Length2+Length3)) - 2*Length1*(Length2+Length3)) / (Projection*Projection) );
        Angle2 = Angle2*val;    //弧度值转角度值

    }
    else if(floatZ > Length0)
    {
        // printf(">");
        Aux = sqrtf( (Projection*Projection) + ((floatZ-Length0)*(floatZ-Length0)) );

        Angle11 = atan((floatZ-Length0) / Projection);
        Angle12 = acos( (Length1*Length1 + Aux*Aux - 2*Length1*Aux) / ((Length2+Length3)*(Length2+Length3)) );
        Angle1 = Angle11 + Angle12;
        Angle1 = Angle1*val;    //弧度值转角度值

        Angle2 = acos( (Length1*Length1 + ((Length2+Length3)*(Length2+Length3)) - 2*Length1*(Length2+Length3)) / (Aux*Aux) );
        Angle2 = Angle2*val;    //弧度值转角度值

    }
    else if(floatZ < Length0)
    {
        // printf("<");
        Aux = sqrtf( (Projection*Projection) + ((Length0-floatZ)*(Length0-floatZ)) );

        Angle11 = atan((Length0-floatZ) / Projection);
        Angle1 = acos( (Length1*Length1 + Aux*Aux - 2*Length1*Aux) / ((Length2+Length3)*(Length2+Length3)) );
        Angle12 = Angle1 - Angle11;
        Angle1 = Angle12;
        Angle1 = Angle1*val;    //弧度值转角度值

        Angle2 = acos( (Length1*Length1 + ((Length2+Length3)*(Length2+Length3)) - 2*Length1*(Length2+Length3)) / (Aux*Aux) );
        Angle2 = Angle2*val;    //弧度值转角度值
    }

    Serial_Printf("\r\n");
    Serial_Printf("Aux:%f\r\n",Aux);
    Serial_Printf("Projection:%f\r\n",Projection);
    Serial_Printf("Angle0:%f\r\n",Angle0);
    Serial_Printf("Angle1:%f\r\n",Angle1);
    Serial_Printf("Angle2:%f\r\n",Angle2);

	Delay_s(1);
	Servo_SetAngle(Angle0,Angle1,Angle2,Angle3);
	return 0;
}

